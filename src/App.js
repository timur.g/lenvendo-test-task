import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
  deleteTextBlock,
  addRandomTextBlock,
  toggleTextBlockSelection,
  toggleComplexTextBlockColor,
} from './ducks/textBlocks'
import SimpleTextBlock from './components/SimpleTextBlock'
import ComplexTextBlock from './components/ComplexTextBlock'

class App extends Component {
  
  render() {
    const blockCount = this.props.textBlocks.length;
    const selectedCount = this.props.textBlocks.reduce( (acc, cur) =>
      {
        if(cur.selected) {
          switch (cur.color) {
            case 'green':
              return {
                ...acc,
                all: acc.all + 1,
                green: acc.green + 1,
              };
            case 'red':
              return {
                ...acc,
                all: acc.all + 1,
                red: acc.red + 1
              };
            default:
              return {
                ...acc,
                all: acc.all + 1,
              }
          }
        } else {
          return acc
        }
      },
      {
        all: 0,
        green: 0,
        red: 0
      });
    
    return (
      <div>
        <h3>Stats</h3>
        <p>blocks counter: {blockCount}</p>
        <p>selected: {selectedCount.all}</p>
        <p>selected green: {selectedCount.green}</p>
        <p>selected red: {selectedCount.red}</p>
        <button
          onClick={this.props.addRandomTextBlock}
        >add random text block</button>
        {this.props.textBlocks.map( item => (
          item.type === 'complex'
            ? <ComplexTextBlock
                key={item.id}
                content={item.content}
                onDelete={() => this.props.deleteTextBlock(item.id)}
                selected={item.selected}
                toggleSelection={() => this.props.toggleTextBlockSelection(item.id)}
                color={item.color}
                toggleColor={() => this.props.toggleComplexTextBlockColor(item.id)}
              />
            : <SimpleTextBlock
                key={item.id}
                content={item.content}
                onDelete={() => this.props.deleteTextBlock(item.id)}
                selected={item.selected}
                toggleSelection={() => this.props.toggleTextBlockSelection(item.id)}
              />
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  textBlocks: state.textBlocks,
});

export default connect(mapStateToProps,
  {
    deleteTextBlock,
    addRandomTextBlock,
    toggleTextBlockSelection,
    toggleComplexTextBlockColor,
  }
)(App);
