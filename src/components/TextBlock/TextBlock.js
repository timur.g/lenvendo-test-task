import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames'
import './styles.css'


class TextBlock extends Component {
  
  /*
  * Prevents firing onClick handler when user doubleClicks.
  * Not completely sure if this is the right thing to do.
  * */
  handleClick = (e) => {
    if(!this._clickTimer) {
      this._clickTimer = setTimeout( () => {
        this.props.onClick(e);
        this._clickTimer = null;
      }, 200);
    } else {
      this._clickTimer = clearTimeout(this._clickTimer);
      if(this.props.onDoubleClick) {
        this.props.onDoubleClick(e);
      }
    }
  };
  
  render() {
    const { content, selected, color } = this.props;
    
    const wrapperClassName = classnames('text-block__wrap', {
      'text-block__wrap_selected': selected,
      'text-block__wrap_colored_green': color === 'green',
      'text-block__wrap_colored_red': color === 'red',
    });
    return (
      <div
        className={wrapperClassName}
        onClick={this.handleClick}
      >
        <p>{content}</p>
        <button
          className={'text-block__delete-button'}
          onClick={this.props.onDelete}
          title={'Delete text block'}
        >x</button>
      </div>
    );
  }
}

TextBlock.propTypes = {
  content: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
  onDoubleClick: PropTypes.func,
};

export default TextBlock;
