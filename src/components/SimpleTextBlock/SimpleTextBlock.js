import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TextBlock from '../TextBlock'

class SimpleTextBlock extends Component {
  render() {
    
    return (
      <TextBlock
        content={this.props.content}
        onDelete={this.props.onDelete}
        selected={this.props.selected}
        onClick={this.props.toggleSelection}
      />
    );
  }
}

SimpleTextBlock.propTypes = {
  content: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
  toggleSelection: PropTypes.func.isRequired,
};

export default SimpleTextBlock;
