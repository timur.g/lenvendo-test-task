import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TextBlock from '../TextBlock'

class ComplexTextBlock extends Component {
  
  handleDelete = (e) => {
    e.stopPropagation();
    const input = window.confirm('Are you sure you want to delete this item?');
    if (input) {
      this.props.onDelete();
    }
  };
  
  render() {
    const { content } = this.props;
    
    return (
      <TextBlock
        content={content}
        onDelete={this.handleDelete}
        selected={this.props.selected}
        onClick={this.props.toggleSelection}
        color={this.props.color}
        onDoubleClick={this.props.toggleColor}
      />
    );
  }
}

ComplexTextBlock.propTypes = {
  content: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
  toggleSelection: PropTypes.func.isRequired,
  color: PropTypes.oneOf(['green', 'red']),
  toggleColor: PropTypes.func.isRequired,
};

export default ComplexTextBlock;
