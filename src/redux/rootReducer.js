import { combineReducers } from 'redux'
import textBlocks from '../ducks/textBlocks'

export default combineReducers({
  textBlocks,
});