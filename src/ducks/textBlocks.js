import { appName } from '../config'
import randomstring from 'randomstring'

/*
* Constants
* */

export const moduleName = 'text-blocks';
const prefix = `${appName}/${moduleName}`;

export const ADD_TEXT_BLOCK = `${prefix}/ADD_TEXT_BLOCK`;
export const DELETE_TEXT_BLOCK = `${prefix}/DELETE_TEXT_BLOCK`;
export const TOGGLE_TEXT_BLOCK_SELECTION = `${prefix}/TOGGLE_TEXT_BLOCK_SELECTION`;
export const TOGGLE_COMPLEX_TEXT_BLOCK_COLOR = `${prefix}/TOGGLE_COMPLEX_TEXT_BLOCK_COLOR`;

/*
* Reducer
* */

const switchColor = color => {
  if(color === 'green') return 'red';
  else if(color === 'red') return 'green';
  else throw new Error('Incorrect color value')
};

export default function textBlocks(state = [], action) {
  const { type, payload } = action;
  
  switch(type) {
    case ADD_TEXT_BLOCK:
      return [...state, payload];
      
    case DELETE_TEXT_BLOCK:
      return state.filter(item => item.id !== payload.id);
      
    case TOGGLE_TEXT_BLOCK_SELECTION:
      return state.map(item => item.id === payload.id ? {...item, selected: !item.selected} : item);
      
    case TOGGLE_COMPLEX_TEXT_BLOCK_COLOR:
      return state.map(item => item.id === payload.id ? {...item, color: switchColor(item.color)} : item);
    
    default: return state;
  }
};

/*
* Action Creators
* */

let nextTextBlockId = 0;

export const addTextBlock = (type, content) => {
  const basePayload = {
    content,
    type,
    id: nextTextBlockId++,
    selected: false,
  };
  
  switch(type) {
    case 'simple':
      return {
        type: ADD_TEXT_BLOCK,
        payload: basePayload,
      };
      
    case 'complex':
      return {
        type: ADD_TEXT_BLOCK,
        payload: {...basePayload, color: 'green'}
      };
      
    default: throw new Error('Incorrect text block type')
  }
};

export const addSimpleTextBlock = addTextBlock.bind(null, 'simple');

export const addComplexTextBlock = addTextBlock.bind(null, 'complex');

export const addRandomTextBlock = () => {
  const rand = Math.random();
  
  if(rand < 0.5) {
    
    return addSimpleTextBlock(randomstring.generate({
      length: 20,
      charset: 'alphabetic',
    }))
  } else {
    
    return addComplexTextBlock(randomstring.generate({
      length: 20,
      charset: 'alphabetic',
    }))
  }
};

export const deleteTextBlock = id => ({
  type: DELETE_TEXT_BLOCK,
  payload: { id }
});

export const toggleTextBlockSelection = id => ({
  type: TOGGLE_TEXT_BLOCK_SELECTION,
  payload: { id }
});

export const toggleComplexTextBlockColor = id => ({
  type: TOGGLE_COMPLEX_TEXT_BLOCK_COLOR,
  payload: { id }
});

